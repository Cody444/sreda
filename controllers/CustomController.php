<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 10.12.2018
 * Time: 20:41
 */

namespace app\controllers;
use yii\web\Controller;

class CustomController extends Controller
{
    protected function  setMeta ($title = null, $description = null, $keywords = null)
    {
         $this->view->title = $title;
         $this->view->registerMetaTag(['name'=> 'keywords', 'content' => $keywords]);
         $this->view->registerMetaTag(['name'=> 'description', 'content' => $description]);
    }
    public static function printr($value)
    {
        echo "<pre>";
        print_r($value);
        echo "</pre>";
    }
}