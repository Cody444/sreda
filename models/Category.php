<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 10.12.2018
 * Time: 19:10
 */

namespace app\models;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'category';
    }
    public function rules()
    {
        return [
            [['title','description','keywords'],'required'],
            [['title','description','keywords'],'string'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'description' => 'Описание',
            'keywords' => 'Ключевые слова',
        ];
    }
}
